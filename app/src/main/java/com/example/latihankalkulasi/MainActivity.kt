package com.unity.wrk.latihankalkulasi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.latihankalkulasi.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var inputSatu: EditText     //membuat parameter
    lateinit var inputDua: EditText
    lateinit var btnTambah: Button
    lateinit var btnKurang: Button
    lateinit var btnKali: Button
    lateinit var btnBagi: Button
    lateinit var hasilHitung: TextView

    override fun onCreate(savedInstanceState: Bundle?) {     //inisialisasi dari parameter
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inputSatu = findViewById(R.id.input_satu)
        inputDua = findViewById(R.id.input_dua)
        btnTambah = findViewById(R.id.btn_tambah)
        btnKurang = findViewById(R.id.btn_kurang)
        btnKali = findViewById(R.id.btn_kali)
        btnBagi = findViewById(R.id.btn_bagi)
        hasilHitung = findViewById(R.id.hasil)

        btnTambah.setOnClickListener(this)
        btnKurang.setOnClickListener(this)
        btnKali.setOnClickListener(this)
        btnBagi.setOnClickListener(this)

    }

    override fun onClick(p0: View) {                         //kondisi dimana memfungsikan tombol memunculkan kondisi when
        val angkaSatu = inputSatu.text.toString().trim()   //val tidak bisa diubah, yg bisa diubah adalah var
        val angkaDua = inputDua.text.toString().trim()
        var inputKosong = false

        when {     //memunculkan allert error/tidak ketika tombol diklik data null/kosong
            angkaSatu.isEmpty() -> {
                inputKosong = true
                inputSatu.error = "Angka ke-1 kosong"
            }
            angkaDua.isEmpty() -> {
                inputKosong = true
                inputDua.error = "Angka ke-2 kosong"
            }
        }

        if (p0.id == R.id.btn_tambah) {
            if (!inputKosong) {
                val hasil = angkaSatu.toDouble() + angkaDua.toDouble()
                hasilHitung.text = hasil.toString()
            }
        }

        if (p0.id == R.id.btn_kurang) {
            if (!inputKosong) {
                val hasil = angkaSatu.toDouble() - angkaDua.toDouble()
                hasilHitung.text = hasil.toString()


            }
        }
        if (p0.id == R.id.btn_kali) {
            if (!inputKosong) {
                val hasil = angkaSatu.toDouble() * angkaDua.toDouble()
                hasilHitung.text = hasil.toString()
            }
        }

        if (p0.id == R.id.btn_bagi) {
            if (!inputKosong) {
                val hasil = angkaSatu.toDouble() / angkaDua.toDouble()
                hasilHitung.text = hasil.toString()

            }
        }

    }

}